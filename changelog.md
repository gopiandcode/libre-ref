# Changelog for LibreRef
## Version 1.0.1
- Fixed major bug causing crashing on launch without config file
## Version 1.0.0
- Initial release
